#!/usr/bin/env zsh

hn=$(hostname)
function my_date () {
    date +"%a %d/%m/%Y %I:%M:%S"
}

function my_bat() {
    notify_when=$(echo "$(date +'%M') % 5" | bc)
    if [[ -d /sys/class/power_supply/BAT1 ]]; then
        bat_level=$( < /sys/class/power_supply/BAT1/capacity )
        bat_stat=$( < /sys/class/power_supply/BAT1/status )
    else
        bat_level=$( < /sys/class/power_supply/BAT0/capacity )
        bat_stat=$( < /sys/class/power_supply/BAT0/status )
    fi
    if [[ $(date +'%S') -eq 30 ]]
    then
        if [[ $( echo "$bat_level < 20" | bc) -eq 1 && (  $notify_when -eq 0 ) && $status != 'Charging'  ]]
        then
            notify-send "Battery low " "$bat_level remaining"
        fi
    fi
    if [[ "$bat_stat" == "Discharging" ]]
    then
        indicator="\xE2\x86\x93"
    else
        indicator="\xE2\x86\x91"
    fi
    echo "B:$bat_level% $indicator"
}

function my_wifi(){
    ssid=$(nmcli | awk '/ connected / {print $4}')
    ip=$(ip --color=never addr show | awk '/([0-9\.]{1,3}){4}\/[0-9]{1,2} brd/ {print $2}')
    strength=$(wifi_strength)
    if [[ $ssid = 'SA_PROFILE' ]]; then
        msg="\xe2\x8c\x82"
    else
        msg=$ssid
    fi
    echo "N:$msg $ip"
}

function wifi_strength() {
    awk '/[0-9]*/ {if ($3/1 != 0) print $3}' /proc/net/wireless
}

function my_load() {
    load=$( ps -eo cmd,%cpu --sort=-%cpu | awk -F ' ' '{sum += $(NF)} END {split (sum, a, "."); print a[1]}' )
    cmd=$(ps -eo cmd,%cpu -U jonathan --sort=-%cpu |  awk  '/[\w\/]/ { n=split( $1, a, "/" ); print a[n] }' | sed '1q')
    temp=$(sensors -f | awk '/Package/ {print $4}')
    echo "Top CMD:$(basename $cmd ) CPU:$load% TEMP:$temp"
}

function my_mail() {
    # $HOME/bin/mailmon
    mail=$( find ~/.config/mail/Inbox/new/ -type f | wc -l| awk '/[0-9]/ {print $1}' )
    if test "$mail" -gt 0; then
        echo " M: $mail "
    fi
}

function my_weather() {
    if [[ -z "$(< "$HOME/.cache/weather")" ]];
    then
        $HOME/bin/weather.py > $HOME/.cache/weather
        weather=' W:'$(< $HOME/.cache/weather)
    fi
    modified_date=$(stat $HOME/.cache/weather | awk '/Modify/ {split($3, arr, ":"); print arr[1]}')
    if [[ ! "$(date +"%H" )" -eq $modified_date ]]
    then
        $HOME/bin/weather.py > $HOME/.cache/weather
        weather=' W:'$(< $HOME/.cache/weather)
    else
        weather=' W:'$(< $HOME/.cache/weather)
    fi
    if [[ "$weather" == " W:error" ]]; then
        $HOME/bin/weather.py > $HOME/.cache/weather
    fi
    echo $weather
}

function my_update() {
    if [[ ! -f /var/spool/cron/jonathan ]]
    then
        notify-send 'check your user crontab'
    fi
    if [[ "$(<  $HOME/.cache/updates | wc -l )" -gt 10 ]] && [[ ! "$(< $HOME/.cache/aur)" ]]; then
        echo " U: $(< $HOME/.cache/updates | wc -l)"
    elif [[ "$(<  $HOME/.cache/updates | wc -l )" -gt 10 ]] && [[ "$(< $HOME/.cache/aur | wc -l )"  ]]; then
        echo " U: $(< $HOME/.cache/updates | wc -l) + $(< $HOME/.cache/aur | wc -l )"
    else
        # do nothing
    fi
}

function my_volume() {
    echo "V:$( amixer -c 0  -- get Master| awk '/Playback [0-9]{0,5} \[[0-9]{0,3}%]/ {print $4 $6}' )"
}

function dbox() {
    box="$(dropbox-cli status)"
    if [[ $box = "Dropbox isn't running!" ]]
    then
        message="off"

    else
        message="$(echo "$box" | head -n1 )"
    fi
    if [[ $message == "Up to date" ]]
    then
        message="\xe2\x9c\x93"
    fi
    echo "D:$message"
}

line="$(my_update)$(my_mail)$(my_weather) $(my_load)"
if [[ -d /sys/class/power_supply/BAT1/ ]]; then
    line="$line $(my_bat)"
elif [[ -d /sys/class/power_supply/BAT0/ ]]; then
    line="$line $(my_bat)"
fi
line="$line $(my_wifi) $(dbox) $(my_volume) $(my_date)"

xsetroot -name $line
if [[ -n "$(pgrep i3)" ]]; then
    echo $line
fi
